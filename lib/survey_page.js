const Page = require('./page')
const {surveySelectors} = require('../helpers/selectors')

Page.prototype.goToSurvey = function(){
    this.goTo('https://www.webankieta.pl/ankieta/389897/zadanie-rekrutacyjne.html')
}

Page.prototype.clickOtherRadioButton = async function(){
    await this.clickElement(surveySelectors.otherCityRadioButton)   
}

Page.prototype.fillCityName = async function(){
    await this.writeText(surveySelectors.textField, 'Rzeszów')
}

Page.prototype.clickNextButton = async function(){
    await this.clickElement(surveySelectors.nextButton)
}

Page.prototype.chooseRandomDifficultyLevel = async function(){
    const elemArray = await this.findElems(surveySelectors.difficultyLevelRadio)
    await (await elemArray[Math.floor(Math.random()*elemArray.length)]).click()
}

Page.prototype.chooseRandomFormOfRecruimentLevel = async function(){
    const elemArray = await this.findElems(surveySelectors.formOfRecruitmentRadio)
    await (await elemArray[Math.floor(Math.random()*elemArray.length)]).click()
}

Page.prototype.clickSendButton = async function(){
    await this.clickElement(surveySelectors.sendButton)
}

Page.prototype.checkMessage = function(){
    return this.getElemText(surveySelectors.completeAlert)
}

Page.prototype.completeSurvey = async function(){
    await this.goToSurvey()    
    await this.clickOtherRadioButton()
    await this.fillCityName()
    await this.clickNextButton()
    await this.chooseRandomDifficultyLevel()
    await this.chooseRandomFormOfRecruimentLevel()
    await this.clickSendButton()    
}

module.exports = Page