const Page =require('../lib/survey_page')
const chai = require("chai")
chai.should()
var page

describe('recruitment task 1', function(){
    this.timeout(50000)

    before(async function(){       
        page = new Page()
    })
    
    after(async function(){        
        await page.quit()
    })

    it('filling survey', async function(){          
        await page.completeSurvey()
        return (await page.checkMessage()).should.equal('Dziękujemy za wypełnienie ankiety')
    })
})