const Page = require('./page')
const {loginSelectors} = require('../helpers/selectors')

Page.prototype.goToPage = function(){
    this.goTo('https://www.webankieta.pl/')
}

Page.prototype.goToLoginPage = async function(){
    await this.clickElement(loginSelectors.homePageLoginButton)
}

Page.prototype.fillEmail = async function(){
    await this.writeText(loginSelectors.emailInput, 'w.rybinski+michal@webankieta.pl')
}

Page.prototype.fillPassword = async function(){
    await this.writeText(loginSelectors.passwordInput, 'EWs9XvpC,9XjEMz')
}

Page.prototype.clickLoginButton = async function(){
    await this.clickElement(loginSelectors.loginButton)
}

Page.prototype.alreadyLoggedIn = async function(){
    try {
        if((await this.getElemText(loginSelectors.alreadyLoggedHeader, 1000))== 'Inna osoba jest zalogowana na to konto'){
            await this.clickElement(loginSelectors.alreadyLoggedContinueButton)
        }
    } catch {
        true
    }    
}

Page.prototype.chooseResults = async function(){
    await this.clickElement(loginSelectors.resultsPage)
}

Page.prototype.chooseAggregatedResults = async function(){
    await this.clickElement(loginSelectors.aggregateResults)
}

Page.prototype.getTotalCompletionCount = function(){
    return this.getElemText(loginSelectors.totalCompletionsCount)
}

Page.prototype.loginIntoService = async function(){
    await this.goToPage()
    await this.goToLoginPage()
    await this.fillEmail()
    await this.fillPassword()
    await this.clickLoginButton() 
    await this.alreadyLoggedIn()   
}

Page.prototype.goToAggregatedResults = async function(){
    await this.loginIntoService()
    await this.chooseResults()
    await this.chooseAggregatedResults()
}

module.exports = Page