const Page = require('../lib/login_page')
const chai = require("chai")
const expect = chai.expect
var page

describe('recruiment task 2', function(){
    this.timeout(50000)

    before(function(){
        page = new Page()
    })

    after(async function(){
        await page.quit()
    })

    it('login to service and check aggregated results', async function(){
        await page.goToAggregatedResults()        
        return expect(parseInt(await page.getTotalCompletionCount())).to.be.above(1)
    })
})