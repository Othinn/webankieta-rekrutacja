const webdriver = require('selenium-webdriver'),
By = webdriver.By,
until = webdriver.until

const Page = function(){
    
    this.driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
    this.driver.manage().window().maximize() 
    const driver = this.driver

    async function findElem(element, timeout= 5000){
        await driver.wait(until.elementLocated(By.xpath(element)), timeout)        
        return driver.findElement(By.xpath(element), 3000)
    }
    
    this.findElems = async function(element){
        await driver.wait(until.elementLocated(By.xpath(element)), 5000)
        return driver.findElements(By.xpath(element))        
    }

    this.goTo = async function(url){
        await driver.get(url)
    }

    this.quit = async function(){
        await driver.quit()
    }      

    this.writeText = async function(el, txt){
        const elem = await findElem(el)         
        await elem.sendKeys(txt)
    }    

    this.getElemText = async function(el, timeout){
        return (await findElem(el, timeout)).getText()
    }

    this.clickElement = async function(el){        
        const elem = await findElem(el)
        await elem.click()     
    }
}

module.exports = Page