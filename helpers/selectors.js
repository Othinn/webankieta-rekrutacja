const surveySelectors = {
    otherCityRadioButton: '//*[@id="question_1"]/fieldset/table/tbody/tr[2]/td[1]/div',
    textField: '//*[@id="question_1"]/fieldset/table/tbody/tr[2]/td[2]/input',
    nextButton: '//*[@id="formNext"]',
    difficultyLevelRadio: '//*[@id="question_2"]/fieldset/div[3]/table[2]/tbody/tr[1]/td/div',
    formOfRecruitmentRadio: '//*[@id="question_2"]/fieldset/div[3]/table[2]/tbody/tr[2]/td/div',
    sendButton: '//*[@id="formFinish"]',
    completeAlert: '//*[@id="changedAjaxArea"]/div/p/strong'    
}

const loginSelectors = {
    homePageLoginButton: '/html/body/div[1]/header/div/nav/ul/li[6]/a',
    emailInput: '//*[@id="email"]',
    passwordInput: '//*[@id="haslo"]',
    loginButton: '//*[@id="signIn"]/section/form/div/p/a',
    alreadyLoggedHeader: '/html/body/div[2]/h2',
    alreadyLoggedContinueButton: '/html/body/div[2]/div/a[2]',
    resultsPage: '//*[@id="saWyniki"]',
    aggregateResults: '/html/body/div[1]/ui-view/analyze-wrapper/main-wrapper/div/div[2]/sub-menu-analyze/sub-menu/div/ul/sub-menu-item[1]/li/a/span',
    totalCompletionsCount: '/html/body/div[1]/ui-view/analyze-wrapper/main-wrapper/div/div[2]/div/ui-view/aggregated-results-wrapper/div/div[2]/survey-wrapper/div/survey-page-wrapper[2]/div/a-r-question/div/div/div[2]/p/span[2]'    
}

module.exports = {surveySelectors, loginSelectors}
